// 
// FILE            kStringSort.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stddef.h>                                // NULL

#include "kbase/kStringSort.h"                     // Own interface



// -----------------------------------------------------------------------------
//
// kStringSort - sort a string, char by char
//
// Resulting string (the value of 's' is overwritten by this function) is sorted
// in increasing order
//
void kStringSort(char* s)
{
  char* current = s;
  char* tmp;
  char* min;

  while (*current != 0)
  {
    tmp = current;
    min = current;

    while (*tmp != 0)
    {
      if (*tmp < *min)
        min = tmp;
      ++tmp;
    }

    if (min != current)
    {
      char tmpChar = *current;

      *current = *min;
      *min     = tmpChar;
    }
    ++current;
  }
}
