// 
// FILE            kFileRead.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stdio.h>                                 // snprintf
#include <stdlib.h>                                // free
#include <sys/types.h>                             // needed for stat
#include <sys/stat.h>                              // struct stat
#include <unistd.h>                                // stat(), open()
#include <fcntl.h>                                 // O_RDONLY

#include "kbase/kBasicLog.h"                       // log macros
#include "kbase/kFileRead.h"                       // Own interface



// -----------------------------------------------------------------------------
//
// kFileRead -
//
int kFileRead(char* base, char* relPath, char** bufP, int* bufLenP)
{
  char         path[1024];
  struct stat  statBuf;

  snprintf(path, sizeof(path), "%s/%s", base, relPath);

  if (stat(path, &statBuf) == -1)
    KBL_RP(1, ("stat(%s)", path));

  char* fileBuf = (char*) malloc(statBuf.st_size + 1);
  int   fd;
  int   nb;

  fd = open(path, O_RDONLY);
  if (fd == -1)
    KBL_RP(2, ("open(%s)", path));
  
  if ((nb = read(fd, fileBuf, statBuf.st_size)) != statBuf.st_size)
  {
    free(fileBuf);

    if (nb == -1)
      KBL_RP(3, ("read(%s)", path));
    else
      KBL_RE(4, ("%s: read %d bytes, expected %d", path, nb, statBuf.st_size));
  }
  
  // Zero terminate the buffer
  fileBuf[statBuf.st_size] = 0;
  
  *bufP    = fileBuf;
  *bufLenP = statBuf.st_size;

  return 0;
}
