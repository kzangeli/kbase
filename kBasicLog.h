#ifndef KBASE_KBASICLOG_H_
#define KBASE_KBASICLOG_H_

// 
// FILE            kBasicLog.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stdio.h>                           // printf
#include <errno.h>                           // errno
#include <string.h>                          // strerror
#include <stdlib.h>                          // exit

#include "kbase/kTypes.h"                    // KBool



// -----------------------------------------------------------------------------
//
// kbVerbose -
//
extern KBool kbVerbose;



// -----------------------------------------------------------------------------
//
// kbNoLineNumbers - no line numbers
//
// Set this variable to KTRUE and all __LINE__ will be converted to 0 in the log output.
// This is useful for the tests, that compares real output with expected output.
// The lines of the source code changes a lot and with this featurte you avoid to use
// a REGEX() just to fix this problem.
//
extern KBool kbNoLineNumbers;


#ifndef KBLOG_ON

//
// all macros empty - no log
//
#define KBL_T(traceLevel, s)
#define KBL_V(s)
#define KBL_M(s)
#define KBL_W(s)
#define KBL_E(s)
#define KBL_P(s)
#define KBL_RV(s)      return
#define KBL_RE(rv, s)  return rv
#define KBL_RP(rv, s)  return rv
#define KBL_X(xc, s)   exit(xc)
#define KBL_FIXME(severity, s)
#define KBL_READS(buf, bytes)
#else
// -----------------------------------------------------------------------------
//
// KBL_T - 
//
#define KBL_T(traceLevel, s)                                           \
do                                                                     \
{                                                                      \
  if (kbTraceV[traceLevel] == KTRUE)                                   \
  {                                                                    \
    int lineNo = (kbNoLineNumbers == KTRUE)? 0 : __LINE__;             \
    printf("%c:%s[%d]: %s: ", 'T', __FILE__, lineNo, __FUNCTION__);    \
    printf s;                                                          \
    printf("\n");                                                      \
    fflush(stdout);                                                    \
  }                                                                    \
} while (0)



// -----------------------------------------------------------------------------
//
// KBL_V - 
//
#define KBL_V(s)                                                       \
do                                                                     \
{                                                                      \
  if (kbVerbose == KTRUE)                                              \
  {                                                                    \
    int lineNo = (kbNoLineNumbers == KTRUE)? 0 : __LINE__;             \
    printf("%c:%s[%d]: %s: ", 'V', __FILE__, lineNo, __FUNCTION__);    \
    printf s;                                                          \
    printf("\n");                                                      \
    fflush(stdout);                                                    \
  }                                                                    \
} while (0)



// -----------------------------------------------------------------------------
//
// KBL_M - 
//
#define KBL_M(s)                                                       \
do                                                                     \
{                                                                      \
  int lineNo = (kbNoLineNumbers == KTRUE)? 0 : __LINE__;               \
  printf("%c:%s[%d]: %s: ", 'M', __FILE__, lineNo, __FUNCTION__);      \
  printf s;                                                            \
  printf("\n");                                                        \
  fflush(stdout);                                                      \
} while (0)



// -----------------------------------------------------------------------------
//
// KBL_W - 
//
#define KBL_W(s)                                                       \
do                                                                     \
{                                                                      \
  int lineNo = (kbNoLineNumbers == KTRUE)? 0 : __LINE__;               \
  printf("%c:%s[%d]: %s: ", 'W', __FILE__, lineNo, __FUNCTION__);      \
  printf s;                                                            \
  printf("\n");                                                        \
  fflush(stdout);                                                      \
} while (0)



// -----------------------------------------------------------------------------
//
// KBL_E - 
//
#define KBL_E(s)                                                       \
do                                                                     \
{                                                                      \
  int lineNo = (kbNoLineNumbers == KTRUE)? 0 : __LINE__;               \
  printf("%c:%s[%d]: %s: ", 'E', __FILE__, lineNo, __FUNCTION__);      \
  printf s;                                                            \
  printf("\n");                                                        \
  fflush(stdout);                                                      \
} while (0)



// -----------------------------------------------------------------------------
//
// KBL_P - 
//
#define KBL_P(s)                                                       \
do                                                                     \
{                                                                      \
  int lineNo = (kbNoLineNumbers == KTRUE)? 0 : __LINE__;               \
  printf("%c:%s[%d]: %s: ", 'E', __FILE__, lineNo, __FUNCTION__);      \
  printf s;                                                            \
  printf(": %s", strerror(errno));                                     \
  printf("\n");                                                        \
  fflush(stdout);                                                      \
} while (0)



// -----------------------------------------------------------------------------
//
// KBL_RE - 
//
#define KBL_RV(s)                                                      \
do                                                                     \
{                                                                      \
  int lineNo = (kbNoLineNumbers == KTRUE)? 0 : __LINE__;               \
  printf("%c:%s[%d]: %s: ", 'E', __FILE__, lineNo, __FUNCTION__);      \
  printf s;                                                            \
  printf("\n");                                                        \
  fflush(stdout);                                                      \
                                                                       \
  return;                                                              \
} while (0)



// -----------------------------------------------------------------------------
//
// KBL_RE - 
//
#define KBL_RE(rv, s)                                                  \
do                                                                     \
{                                                                      \
  int lineNo = (kbNoLineNumbers == KTRUE)? 0 : __LINE__;               \
  printf("%c:%s[%d]: %s: ", 'E', __FILE__, lineNo, __FUNCTION__);      \
  printf s;                                                            \
  printf("\n");                                                        \
  fflush(stdout);                                                      \
                                                                       \
  return rv;                                                           \
} while (0)



// -----------------------------------------------------------------------------
//
// KBL_RP - 
//
#define KBL_RP(rv, s)                                                  \
do                                                                     \
{                                                                      \
  int lineNo = (kbNoLineNumbers == KTRUE)? 0 : __LINE__;               \
  printf("%c:%s[%d]: %s: ", 'E', __FILE__, lineNo, __FUNCTION__);      \
  printf s;                                                            \
  printf(": %s", strerror(errno));                                     \
  printf("\n");                                                        \
  fflush(stdout);                                                      \
                                                                       \
  return rv;                                                           \
} while (0)



// -----------------------------------------------------------------------------
//
// KBL_X - 
//
#define KBL_X(xc, s)                                                   \
do                                                                     \
{                                                                      \
  int lineNo = (kbNoLineNumbers == KTRUE)? 0 : __LINE__;               \
  printf("%c:%s[%d]: %s: ", 'X', __FILE__, lineNo, __FUNCTION__);      \
  printf s;                                                            \
  printf("\n");                                                        \
  fflush(stdout);                                                      \
                                                                       \
  exit(xc);                                                            \
} while (0)



// -----------------------------------------------------------------------------
//
// KBL_FIXME - 
//
#define KBL_FIXME(severity, s)                                         \
do                                                                     \
{                                                                      \
  int lineNo = (kbNoLineNumbers == KTRUE)? 0 : __LINE__;               \
  printf("%c:%s[%d]: %s: ", 'F', __FILE__, lineNo, __FUNCTION__);      \
  printf s;                                                            \
  printf("\n");                                                        \
  fflush(stdout);                                                      \
} while (0)



// -----------------------------------------------------------------------------
//
// KBL_READS - 
//
#define KBL_READS(buf, bytes)                                          \
do                                                                     \
{                                                                      \
  printf("KBL_READS is not defined");                                  \
} while (0)

#endif  // KBLOG_ON

#endif  // KBASE_KBASICLOG_H_
