#ifndef KBASE_KSRTEQ_H_
#define KBASE_KSRTEQ_H_

// 
// FILE            kStrEq.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include "kbase/kTypes.h"               // KBool



// -----------------------------------------------------------------------------
//
// kStrEq -
//
KBool kStrEq(const char* s1, const char* s2)
{
  while ((*s1 != 0) && (*s2 != 0))
  {
    if (*s1 != *s2)
      return KFALSE;
    ++s1;
    ++s2;
  }

  if (*s1 != *s2)
    return KFALSE;

  return KTRUE;
}

#endif  // KBASE_KSRTEQ_H_
