// 
// FILE            kTime.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KBASE_KTIME_H_
#define KBASE_KTIME_H_

#include <time.h>                            // struct timespec



// -----------------------------------------------------------------------------
//
// kTimeGet - get current time 
//
extern int kTimeGet(struct timespec* tP);



// -----------------------------------------------------------------------------
//
// kTimeDiff - calculate the difference between two timespecs
//
extern void kTimeDiff(struct timespec* startP, struct timespec* endP, struct timespec* diffP, float* fP);



// -----------------------------------------------------------------------------
//
// kTimeAccumulate - accumulate timespecs
//
extern void kTimeAccumulate(struct timespec* accumulatedP, struct timespec* partP, float* fP);

#endif  // KBASE_KTIME_H_
