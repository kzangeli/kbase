// 
// FILE            kStringSplit.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stddef.h>                                // NULL

#include "kbase/kStringSplit.h"                    // Own interface



// -----------------------------------------------------------------------------
//
// kStringSplit - split a string into a vector
//
// NOTE
//   The initial string (char* s) is destroyed, as each occurrence of the DELIMITER (char d)
//   is replaced by a ZERO and an item in the string vector (char** sVec) points
//   inside the initial string (char* s).
//
//   This is very efficient as no allocation is needed, however, it has two (major) drawbacks:
//     1. The initial string must be writable.
//     2. The initial string must be copied before calling kStringSplit if it needs to be maintained intact.
//
int kStringSplit(char* s, char d, char** sVec, int sVecLen)
{
  int sIx = 0;

  // No/Empty string?
  if ((s == NULL)  || (*s == 0))
    return 0;

  sVec[sIx++] = s;

  while (*s != 0)
  {
    if (*s == d)
    {
      *s = 0;
      ++s;

      if (sIx < sVecLen)
        sVec[sIx] = s;
      else
        return sIx;  // FIXME: Silently stop or return an error?

      ++sIx;
    }

    ++s;
  }

  return sIx;
}
