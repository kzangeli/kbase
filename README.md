# kbase - basic functionality for the k suite of libs

## Table of Contents

## Background
While implementing my JSON parser - the kjson library, together with a few more "K-libs" I decided to move out
common functions to be easy to reuse from any future repo.
This library is the collection of utility functions that were implemented during these projects.

## Install
% make di  # di == debug install

## Usage

### kCharChecksum
```
#include "kbase/kCharChecksum.h"
char kCharChecksum(void* buf, int bufLen)
```
`kCharChecksum` performs a simple "checksum" of 'char' of a sized buffer.
The checfksum is returned by the function.
This "checksum" is simply the sum of each of the characters in `buf`.

### kCharCount
```
#include "kbase/kCharCount.h"
int kCharCount(char* haystack, char needle)
```
`kCharCount` counts the number of occurences of `needle` in `haystack`.
`haystack` is not checked for NULL, if it is NULL, this function crashes your program.
This is how the C std library works and it is good, for performance issues.

### kFileRead
```
int kFileRead(char* base, char* relPath, char** bufP, int* bufLenP)
```
`kFileRead` assembles a path by joining the input parameters `base` and `relPath`,
opens that file and stores the entire file content inside an allocated buffer that it
sets the output parameter `bufP` to reference. The length of this string is stored in the
second output parameter `bufLenP`.


### kFloatTrim
```
void kFloatTrim(char* floatString)
```
`kFloatTrim` has one input parameter - `floatString` - a string that contains a float as ASCII.
If no dot is found inside `floatString` the the string is left intact.
If a dot *is* found, then the string is "a valid floting number" and trailing zeroes are nuller out.
I.e., the string "14.12300" is changed to "14.123".


### kProgName
```
char* kProgName(char* argv0)
```
`kProgName` takes as input a string, typically `argv[0]` from `main()`, assuming it's a path to an executable,
and strips the string of any directories.
A pointer to this "clean" file name is returned, no allocation is used.


### kStringSort
```
void kStringSort(char* s)
```
`kStringSort` sorts a string in alphabetical order, overwriting the incoming buffer `s`.


### kStringSplit
```
int kStringSplit(char* s, char d, char** sVec, int sVecLen)
```
`kStringSplit` splits a string into a vector of strings.

The initial string (char* s) is destroyed, as each occurrence of the DELIMITER (char d)
is replaced by a ZERO and an item in the string vector (char** sVec) points
inside the initial string (char* s).

This is very efficient as no allocation is needed, however, it has two drawbacks:
  1. The initial string *must* be writable.
  2. If the initial string must be maintained intact, it must be copied before calling `kStringSplit`.


### kTimeGet
```
int kTimeGet(struct timespec* tP)
```
`ktimeGet` extracts the current time using the call `clock_gettime` with clock `CLOCK_THREAD_CPUTIME_ID`,
and stores the value in the output parameter `tP`.


### kTimeDiff
```
void kTimeDiff(struct timespec* startP, struct timespec* endP, struct timespec* diffP, float* fP)
```
`kTimeDiff` calculates the difference between two timespecs and stores the result in the two output parameters `diffP` and `fP`.
The input parameters `startP` and `endP` typically have been set using `kTimeGet`.


### kTimeAccumulate
```
void kTimeAccumulate(struct timespec* accumulatedP, struct timespec* partP, float* fP)
```
`kTimeAccumulate` adds the timespec `partP` to the "accumulator" `accumulatedP` and if a third paramteer `fP` is non-NULL,
then the resulting accumulated time is returned as a float (in the unit `seconds`).


## License
[Apache 2.0](LICENSE) © 2016-2020 Ken Zangelin
