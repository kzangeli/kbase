// 
// FILE            kTime.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <time.h>                            // struct timespec

#include "kbase/kTime.h"                     // Own interface



// -----------------------------------------------------------------------------
//
// kTimeGet - get current time 
//
int kTimeGet(struct timespec* tP)
{
  return clock_gettime(CLOCK_REALTIME, tP);
}



// -----------------------------------------------------------------------------
//
// kTimeDiff - calculate the difference between two timespecs
//
void kTimeDiff(struct timespec* startP, struct timespec* endP, struct timespec* diffP, float* fP)
{
  diffP->tv_sec  = endP->tv_sec  - startP->tv_sec;
  diffP->tv_nsec = endP->tv_nsec - startP->tv_nsec;

  if (diffP->tv_nsec < 0)
  {
    diffP->tv_sec  -= 1;
    diffP->tv_nsec += 1000000000;
  }
  
  if (fP != NULL)
    *fP = diffP->tv_sec + ((float) diffP->tv_nsec) / 1000000000;
}



// -----------------------------------------------------------------------------
//
// kTimeAccumulate - accumulate timespecs
//
void kTimeAccumulate(struct timespec* accumulatedP, struct timespec* partP, float* fP)
{
  accumulatedP->tv_sec  += partP->tv_sec;
  accumulatedP->tv_nsec += partP->tv_nsec;

  if (accumulatedP->tv_nsec < 0)
  {
    accumulatedP->tv_sec  -= 1;
    accumulatedP->tv_nsec += 1000000000;
  }
  
  if (fP != NULL)
    *fP = accumulatedP->tv_sec + ((float) accumulatedP->tv_nsec) / 1000000000;
}
