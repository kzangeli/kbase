#ifndef KBASE_KLIBLOG_H_
#define KBASE_KLIBLOG_H_

// 
// FILE            kLibLog.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 



// -----------------------------------------------------------------------------
//
// KLogFunction - 
//
typedef void (*KLogFunction)
(
  int          severity,              // 1: Error, 2: Warning, 3: Info, 4: Msg, 5: Verbose, 6: Trace
  int          level,                 // Trace level || Error code || Info Code
  const char*  fileName,
  int          lineNo,
  const char*  functionName,
  const char*  format,
  ...
);



#ifdef K_LOG_ON
// -----------------------------------------------------------------------------
//
// KLOG_E
//
#define KLOG_E(...) kjLogFunction(1, 0, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)



// -----------------------------------------------------------------------------
//
// KLOG_RE
//
#define KLOG_RE(retVal, ...) do { kjLogFunction(1, 0, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__); return retVal; } while (0)



// -----------------------------------------------------------------------------
//
// KLOG_W
//
#define KLOG_W(...) kjLogFunction(2, 0, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)



// -----------------------------------------------------------------------------
//
// KLOG_I
//
#define KLOG_I(...) kjLogFunction(3, 0, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)



// -----------------------------------------------------------------------------
//
// KLOG_M
//
#define KLOG_M(...) kjLogFunction(4, 0, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)



// -----------------------------------------------------------------------------
//
// KLOG_V
//
#define KLOG_V(...) kjLogFunction(5, 0, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)



// -----------------------------------------------------------------------------
//
// KLOG_T
//
#define KLOG_T(tLevel, ...) kjLogFunction(6, tLevel, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)



#else

#define KLOG_E(...)
#define KLOG_RE(retVal, ...) return retVal
#define KLOG_W(...)
#define KLOG_I(...)
#define KLOG_M(...)
#define KLOG_V(...)
#define KLOG_T(tLevel, ...)
#endif



// -----------------------------------------------------------------------------
//
// kLogFunction - 
//
extern KLogFunction kLogFunction;

#endif  // KBASE_KLIBLOG_H_
