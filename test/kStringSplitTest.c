// 
// FILE            kStringSplitTest.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stdio.h>
#include <string.h>



int kStringSplit(char* s, char d, char** sVec, int sVecLen)
{
  int sIx = 0;

  // No/Empty string?
  if ((s == NULL)  || (*s == 0))
    return 0;

  sVec[sIx++] = s;

  while (*s != 0)
  {
    if (*s == d)
    {
      *s = 0;
      ++s;

      if (sIx < sVecLen)
        sVec[sIx] = s;
      else
        return sIx;

      ++sIx;
    }

    ++s;
  }

  return sIx;
}



int main(int argC, char* argV[])
{
  char* outV[4];
  int   items;
  char  in[64];
  int   ix;

  strcpy(in, "name,type,value,metadata");

  items = kStringSplit(in, ',', outV, 4);

  printf("%d items\n", items);
  
  for (ix = 0; ix < items; ix++)
    printf("item %d: '%s'\n", ix, outV[ix]);
  return 0;
}
