// 
// FILE            atoh.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stdlib.h>            // exit, strtoul
#include <string.h>            // strcmp
#include <stdio.h>             // printf
#include <ctype.h>             // isprint



// -----------------------------------------------------------------------------
//
// main - 
//
int main(int argC, char* argV[])
{
  if ((argC == 1) || ((argC == 2) && (strcmp(argV[1], "-u") == 0)))
  {
    printf("Usage: atoh -u (to see this text)\n");
    printf("       atoh -a (to see a list of all printable chars)\n");
    printf("       atoh <char1> <char2> <char3> ... <charN>\n");
    exit(1);
  }
  
  if ((argC == 2) && (strcmp(argV[1], "-a") == 0))
  {
    int ix;

    for (ix = 1; ix <= 0xFF; ix++)
    {
      if (isprint(ix))
        printf("%c: 0x%x\n", ix, ix);
    }
  }
  else
  {
    int ix;

    for (ix = 1; ix < argC; ix++)
    {
      int len = strlen(argV[ix]);
      int jx;

      for (jx = 0; jx < len; ++jx)
        printf("%c: 0x%x\n", argV[ix][jx], argV[ix][jx]);
    }
  }

  return 0;
}
