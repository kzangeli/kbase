// 
// FILE            kStringArrayJoin.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2020 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include <string.h>                                // strlen, strcpy
#include <stddef.h>                                // NULL

#include "kbase/kStringArrayJoin.h"                // Own interface



// -----------------------------------------------------------------------------
//
// kStringArrayJoin - join a string into a vector
//
// NOTE:
//   A bit unsafe, please make sure 'output' has room enough ...
//   FIXME: add size of 'output' as second parameter and make the function safe!
//
void kStringArrayJoin(char* output, char** stringV, int vecItems, const char* separator)
{
  int outputIx      = 0;
  int separatorLen  = (separator != NULL)? strlen(separator) : 0;

  int ix;
  for (ix = 0; ix < vecItems; ix++)
  {
    strcpy(&output[outputIx], stringV[ix]);
    outputIx += strlen(stringV[ix]);

    // Add 'separator', except for the last round
    if ((ix != vecItems - 1) && (separator != NULL))
    {
      strcpy(&output[outputIx], separator);
      outputIx +=	separatorLen;
    }
  }
}
