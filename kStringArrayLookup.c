// 
// FILE            kStringArrayLookup.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2020 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include <string.h>                                // strlen, strcpy
#include <stddef.h>                                // NULL

#include "kbase/kStringArrayLookup.h"              // Own interface



// -----------------------------------------------------------------------------
//
// kStringArrayLookup - lookup a string in a string vector
//
int kStringArrayLookup(char** stringV, int vecItems, char* needle)
{
  int ix;
  for (ix = 0; ix < vecItems; ix++)
  {
    if (strcmp(stringV[ix], needle) == 0)
      return ix;
  }

  return -1;
}
