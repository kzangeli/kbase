// 
// FILE            kCharCount.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KBASE_KSTRING_H_
#define KBASE_KSTRING_H_



// -----------------------------------------------------------------------------
//
// kCharCount -
//
extern int kCharCount(char* haystack, char needle);

#endif  // KBASE_KSTRING_H_
