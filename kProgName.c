// 
// FILE            kProgName.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <string.h>                          // strlen

#include "kbase/kProgName.h"                 // Own interface



// -----------------------------------------------------------------------------
//
// kProgName - return program name, without any directories in the string
//
char* kProgName(char* argv0)
{
  //
  // 00. Validity checks
  //
  if (argv0 == NULL)
    return "NO PROGNAME";

  if (argv0[0] == 0)
    return "EMPTY PROGNAME";

  //
  // 01. Position cP at the last char of the string argv0
  //
  char* cP = &argv0[strlen(argv0) - 1];


  //
  // 02. Search 'backwards' until '/' is found (or not)
  //
  while (cP >= argv0)
  {
    if (*cP == '/')
      return &cP[1];

    --cP;
  }


  //
  // 03. No '/' found - just return the string that was input to the function
  //
  return argv0;
}
