// 
// FILE            kProgName.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KBASE_KPROGNAME_H_
#define KBASE_KPROGNAME_H_



// -----------------------------------------------------------------------------
//
// kProgName - return program name, without any directories in the string
//
// NOTE
//   This function only returns a pointerto where the 'clean' program name starts.
//   The function DOES NOT allocate any memory for it.
//   Allocation would be responsibility of the caller, if needed.
//   Why?
//   Well, the normal usage of this function is to send in argV[0], right in the beginning
//   of the 'main' function. This string leves on the stack as long as the program is alive,
//   so, no allocation is needed, at least not in the intended use of this function.
//   If used in other circumstances, what yhis function returns might be a good candidate
//   for strdup ...
//
extern char* kProgName(char* argv0);

#endif  // KBASE_KPROGNAME_H_
