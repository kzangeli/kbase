#ifndef KBASE_KSTRINGARRAYLOOKUP_H_
#define KBASE_KSTRINGARRAYLOOKUP_H_

// 
// FILE            kStringArrayLookup.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2020 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 



// -----------------------------------------------------------------------------
//
// kStringArrayLookup - lookup a string in a string vector
//
extern int kStringArrayLookup(char** stringV, int vecItems, char* needle);

#endif  // KBASE_KSTRINGARRAYLOOKUP_H_
