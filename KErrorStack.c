// 
// FILE            KErrorStack.c
//
// AUTHOR          Ken Zangelin
//
// Copyright 2020 Ken Zangelin
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include <strings.h>                               // bzero
#include <stdlib.h>                                // calloc, realloc
#include <stdio.h>                                 // printf

#include "kbase/KErrorStack.h"                     // Own interface



// -----------------------------------------------------------------------------
//
// kErrorInit -
//
void kErrorInit(KErrorStack* esP, int maxErrorItems)
{
  bzero(esP, sizeof(KErrorStack));
  esP->errorV    = (KErrorItem*) calloc(maxErrorItems, sizeof(KErrorItem));
  esP->errorSize = maxErrorItems;
}



// -----------------------------------------------------------------------------
//
// kErrorPushFunction -
//
void kErrorPushFunction
(
  KErrorStack*  esP,
  char*         file,
  int           line,
  char*         function,
  int           code,
  char*         title,
  char*         where,
  char*         detail
)
{
  if (esP->ix >= esP->errorSize)
  {
    // No room - let's add another 5
    esP->errorSize += 5;
    esP->errorV     = realloc(esP->errorV, esP->errorSize * sizeof(KErrorItem));
  }

  KErrorItem* errorP  = &esP->errorV[esP->ix];

  errorP->file      = file;
  errorP->line      = line;
  errorP->function  = function;
  errorP->code      = code;
  errorP->where     = where;
  errorP->detail    = detail;
}



// -----------------------------------------------------------------------------
//
// kErrorFlushToScreen -
//
void kErrorFlushToScreen(KErrorStack* esP)
{
  if (esP->ix == 0)  // False Alarm?
    return;

  esP->errorCounter += 1;  // As it starts from 0, it is inceremented before reporting

  for (int ix = 0; ix < esP->ix; ix++)
  {
    KErrorItem* errorP = &esP->errorV[ix];

    printf("Error %d: %04d: %s[%d]:%s: %s|%s|: %s\n",
           esP->errorCounter,
           errorP->code,
           errorP->file,
           errorP->line,
           errorP->function,
           errorP->title,
           errorP->where,
           errorP->detail);
  }

  esP->ix = 0;
}



// -----------------------------------------------------------------------------
//
// kErrorFree -
//
void kErrorFree(KErrorStack* esP)
{
  if (esP->errorV != NULL)
    free(esP->errorV);
  esP->errorV = NULL;
}
