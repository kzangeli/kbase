// 
// FILE            kStringReplace.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2020 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <string.h>                      // strncmp, strlen, strcpy



// -----------------------------------------------------------------------------
//
// kStringReplace -
//
char* kStringReplace(char* in, char* replace, char* with, char* out, int outLen)
{
  int inIx       = 0;
  int outIx      = 0;
  int replaceLen = strlen(replace);
  int withLen    = strlen(with);

  while (in[inIx] != 0)
  {
    if (strncmp(&in[inIx], replace, replaceLen) == 0)
    {
      inIx += replaceLen;
      if (withLen != 0)
      {
        strcpy(&out[outIx], with);
        outIx += withLen;
      }
    }
    else
      out[outIx++] = in[inIx++];
  }

  out[outIx] = 0;

  return out;
}
