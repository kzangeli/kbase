#ifndef KBASE_KERRORSTACK_H_
#define KBASE_KERRORSTACK_H_

// 
// FILE            KErrorStack.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2020 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 



// -----------------------------------------------------------------------------
//
// KErrorItem -
//
typedef struct KErrorItem
{
  char*  file;
  int    line;
  char*  function;
  int    code;
  char*  title;
  char*  where;
  char*  detail;
} KErrorItem;



// -----------------------------------------------------------------------------
//
// KErrorStack -
//
typedef struct KErrorStack
{
  KErrorItem*  errorV;
  int          errorSize;
  int          ix;                // Current index in the Error Stack (errorV)
  int          errorCounter;      // Accumulated number of errors (number of error stacks produced)
} KErrorStack;



// -----------------------------------------------------------------------------
//
// kErrorPush -
//
#define kErrorPush(errorStackP, code, title, where, detail)                                      \
do                                                                                               \
{                                                                                                \
  kErrorPushFunction(__FILE__, __LINE__, __FUNCTION__, errorStackP, code, title, where, detail); \
} while (0)
  


// -----------------------------------------------------------------------------
//
// kErrorInit -
//
extern void kErrorInit(KErrorStack* esP, int maxErrorItems);



// -----------------------------------------------------------------------------
//
// kErrorPushFunction -
//
extern void kErrorPushFunction
(
  KErrorStack*  esP,
  char*         file,
  int           line,
  char*         function,
  int           code,
  char*         title,
  char*         where,
  char*         detail
);



// -----------------------------------------------------------------------------
//
// kErrorFlushToScreen -
//
extern void kErrorFlushToScreen(KErrorStack* esP);



// -----------------------------------------------------------------------------
//
// kErrorFree -
//
extern void kErrorFree(KErrorStack* esP);

#endif  // KBASE_KERRORSTACK_H_
