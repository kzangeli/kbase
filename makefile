# 
# FILE            makefile
# 
# AUTHOR          Ken Zangelin
# 
# Copyright 2019 Ken Zangelin
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with theLicense.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
LIB_SO        = libkbase.so
LIB           = libkbase.a
CC            = gcc
INCLUDE       = -I..
DFLAGS        = -DANSI -DK_LOG_ON
CFLAGS        = -g -Wall -fPIC -Wno-unused-function $(DFLAGS) $(INCLUDE)
LIB_SOURCES   = kInit.c              \
                kCharCount.c         \
                kCharChecksum.c      \
                kBasicLog.c          \
                kProgName.c          \
                kStringSplit.c       \
                kStringArrayJoin.c   \
                kStringArrayLookup.c \
                kStringSort.c        \
                kTime.c              \
                kFileRead.c          \
                kbaseVersion.c       \
                kFloatTrim.c         \
                kLibLog.c            \
                kFileSuffixExtract.c \
                KErrorStack.c        \
                kStringReplace.c
LIB_OBJS      = $(LIB_SOURCES:c=o)
TEST          = kTest
TEST_SOURCES  = kTest.c
TEST_OBJS     = $(TEST_SOURCES:c=o)

HTOA          = tools/htoa/htoa
HTOA_SOURCES  = tools/htoa/htoa.c
HTOA_OBJS     = $(HTOA_SOURCES:c=o)

ITOH          = tools/itoh/itoh
ITOH_SOURCES  = tools/itoh/itoh.c
ITOH_OBJS     = $(ITOH_SOURCES:c=o)

HTOI          = tools/htoi/htoi
HTOI_SOURCES  = tools/htoi/htoi.c
HTOI_OBJS     = $(HTOI_SOURCES:c=o)

ATOI          = tools/atoi/atoi
ATOI_SOURCES  = tools/atoi/atoi.c
ATOI_OBJS     = $(ATOI_SOURCES:c=o)

ATOH          = tools/atoh/atoh
ATOH_SOURCES  = tools/atoh/atoh.c
ATOH_OBJS     = $(ATOH_SOURCES:c=o)

all: $(LIB_SO) $(LIB) $(TEST) $(HTOA) $(ITOH) $(HTOI) $(ATOI) $(ATOH)

clean:
						rm -f $(LIB_OBJS)
						rm -f $(LIB_SO)
						rm -f $(LIB)
						rm -f $(TEST) $(TEST_OBJS)
						rm -f $(HTOA) $(HTOA_OBJS)
						rm -f $(HTOI) $(HTOI_OBJS)
						rm -f $(HTOI) $(HTOI_OBJS)
						rm -f $(ATOI) $(ATOI_OBJS)
						rm -f $(ATOH) $(ATOH_OBJS)
						rm -f $(ITOH) $(ITOH_OBJS)

i:          install

install:    all
						@if [ ! -d bin ]; then mkdir bin; fi
						cp $(TEST) bin/
						cp $(HTOA) bin/
						cp $(ITOH) bin/
						cp $(HTOI) bin/
						cp $(ATOI) bin/
						cp $(ATOH) bin/

di:         install

ci:         clean install

$(LIB):			$(LIB_OBJS) $(LIB_SOURCES)
						ar r $(LIB) $(LIB_OBJS)
						ranlib $(LIB)

$(LIB_SO):	$(LIB_OBJS) $(LIB_SOURCES)
						$(CC) -shared $(LIB_OBJS) -o $(LIB_SO)

$(TEST):		$(TEST_OBJS) $(LIB)
						$(CC) -o $(TEST) $(TEST_OBJS) $(LIB)  $(LIBS) -lrt

$(HTOA):		$(HTOA_OBJS) $(LIB)
						$(CC) -o $(HTOA) $(HTOA_OBJS)

$(ITOH):		$(ITOH_OBJS) $(LIB)
						$(CC) -o $(ITOH) $(ITOH_OBJS)

$(HTOI):		$(HTOI_OBJS) $(LIB)
						$(CC) -o $(HTOI) $(HTOI_OBJS)


%.o: %.c
						$(CC) $(CFLAGS) -c $^ -o $@

%.i: %.c
						$(CC) $(CFLAGS) -c $^ -E > $@

%.cs: %.c
						$(CC) -Wa,-adhln -g $(CFLAGS)  $^  > $@
