// 
// FILE            kCharCount.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include "kbase/kCharCount.h"                // Own interface



// -----------------------------------------------------------------------------
//
// kCharCount - count number of occurences of 'needle' in 'haystack'
//
// NOTE
//   haystack is not checked for NULL, if it is NULL, this function crashes your program,
//   This is how the C std library works and it is good, for performance issues.
//
int kCharCount(char* haystack, char needle)
{
  int hits = 0;

  while (*haystack != 0)
  {
    if (*haystack == needle)
      ++hits;

    ++haystack;
  }

  return hits;
}
