// 
// FILE            kMacros.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KBASE_KMACROS_H_
#define KBASE_KMACROS_H_

#include "kbase/kTypes.h"               // KBool, KFALSE, KTRUE


// -----------------------------------------------------------------------------
//
// K_VEC_SIZE - the size of an array, in items
//
#define K_VEC_SIZE(a)  (sizeof(a) / sizeof(a[0]))



// -----------------------------------------------------------------------------
//
// K_MAX - max value
//
#define K_MAX(a, b)  ((a) > (b)? (a) : (b))



// -----------------------------------------------------------------------------
//
// K_MIN - min value
//
#define K_MIN(a, b)  ((a) < (b)? (a) : (b))



// -----------------------------------------------------------------------------
//
// K_FT - 
//
#define K_FT(b)  (((b) == KFALSE)? "false" : ((b) == KTRUE)? "true" : "NOR KFALSE NOR KTRUE")



// -----------------------------------------------------------------------------
//
// K_SET - 
//
#define K_SET(b)  (((b) == KFALSE)? "unset" : ((b) == KTRUE)? "set" : "Nor Set nor Unset")

#endif  // KBASE_KMACROS_H_
