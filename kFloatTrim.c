//
// FILE            kFloatTrim.c
//
// AUTHOR          Ken Zangelin
//
// Copyright 2019 Ken Zangelin
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include <string.h>                          // strlen

#include "kbase/kTypes.h"                    // KBool
#include "kbase/kFloatTrim.h"                // Own interface



// -----------------------------------------------------------------------------
//
// kFloatTrim - remove trailing zeroes in decimal part
//
void kFloatTrim(char* floatString)
{
  //
  // 1. Is there a decimal part?
  //
  char* cP          = floatString;
  KBool decimalPart = KFALSE;

  while (*cP != 0)
  {
    if (*cP == '.')
    {
      decimalPart = KTRUE;
      break;
    }
    ++cP;
  }

  if (decimalPart == KFALSE)
    return;

  //
  // 2. Remove last char if it's a '0'
  //
  int lastCharIx = strlen(floatString) - 1;
  while (floatString[lastCharIx] == '0')
  {
    floatString[lastCharIx] = 0;
    --lastCharIx;
  }

  //
  // 3. Remove last char if it's a '.'
  //
  if (floatString[lastCharIx] == '.')
    floatString[lastCharIx] = 0;
}
