// 
// FILE            kFileSuffixExtract.c
//
// AUTHOR          Ken Zangelin
//
// Copyright 2020 Ken Zangelin
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include <string.h>                           // strlen, strdup

#include "kbase/kTypes.h"                     // KBool, KFALSE, KTRUE



// -----------------------------------------------------------------------------
//
// kFileSuffixExtract -
//
char* kFileSuffixExtract(char* fileName, int len, KBool destructive)
{
  char* endP;

  if (len == -1)
    len = strlen(fileName);
  endP = &fileName[len - 1];

  while (*endP != '.')
  {
    --endP;
    if (endP <= fileName)
      return NULL;
  }

  if (destructive == KFALSE)  // Not destructive - can't NULL out the dor at *endP . must allocate
    return strdup(&endP[1]);

  *endP = 0;
  return &endP[1];
}
